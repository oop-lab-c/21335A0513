import java.io.*;
class Main implements Runnable
{
    public static void main(String[] args)
    {
        Main obj = new Main();
        Thread obj1 = new Thread(obj, "the Thread object is created");
        obj1.start();
        System.out.println(obj1.getName());
    }
    @Override public void run()
    {
        System.out.println("Run method Called");
    }
}
