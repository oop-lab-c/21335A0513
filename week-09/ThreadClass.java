import java.io.*;
class base extends Thread
{
    public void run()
    {
        System.out.println("this is extending Thread class to base class");
    }
}
class Main{
    public static void main(String[] args)
    {
        base obj = new base();
        obj.start();
    }
}
