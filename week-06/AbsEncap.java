class AccessSpecierDemo
{
    private
    int privar;
    protected
    int prover;
    public
    int pubver;
    public
    void setVar(int priValue ,int proValue,int pubValue){
       privar=priValue;
       prover=proValue;
       pubver=pubValue;
    }
    public 
     void getVar(){
        System.out.println("Private value is :"+privar);
        System.out.println("Protected value is :"+prover);
        System.out.println("Public value is :"+pubver);
    }
}
class AbsEncap{
    public static void main(String[] args) {
        AccessSpecierDemo of=new AccessSpecierDemo();
        of.setVar(100,200,300);
        of.getVar();
    }
}
