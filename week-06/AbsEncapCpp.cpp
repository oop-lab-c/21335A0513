#include<iostream>
using namespace std;
class AccessSpecierDemo
{
    private:
    int privar;
    protected:
    int prover;
    public:
    int pubver;
    public: void setVar(int priValue ,int proValue,int pubValue){
       privar=priValue;
       prover=proValue;
       pubver=pubValue;
    }
    public : void getVar(){
        cout<<"Private value is :"<<privar<<endl;
        cout<<"Protected value is :"<<prover<<endl;
        cout<<"Public value is :"<<pubver<<endl;
    }
};

int main(){
    AccessSpecierDemo obj;
    obj.setVar(22,43,55);
    obj.getVar();
    return 0;
}
