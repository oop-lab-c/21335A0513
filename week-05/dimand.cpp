#include<iostream>
using namespace std;
class super{
    public:
    void rap(){
        cout<<"this is first class"<<endl;

    }
};
class low:virtual public super{
    public:
    void rap1(){
        cout<<"this is second class inherite the fist class"<<endl;     
    }
};
class high:virtual public super{
    public:
    void rap2(){
       cout<<"this is third class inherite the first class"<<endl;
    }
};
class base:virtual public low,virtual public high{
    public:
    void rap3(){
       cout<<"this is last class it inherite second and third class"<<endl;
    }
};
int main(){
base d;
d.rap();
d.rap1();
d.rap2();
d.rap3();
}
