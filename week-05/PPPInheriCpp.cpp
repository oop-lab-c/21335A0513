#include <iostream>
using namespace std;

class Base {
  private:
    int a = 1;

  protected:
    int b = 2;

  public:
    int c = 3;
    int getPVT() {
      return a;
    }
};

class PrivateDerived : private Base {
  public:
    int getProt() {
      return b;
    }
    int getPub() {
      return c;
    }
};

int main() {
  PrivateDerived dd;
  cout << "Private cannot be accessed." << endl;
  cout << "Protected = " << dd.getProt() << endl;
  cout << "Public = " << dd.getPub() << endl;
  return 0;
}
