#include <iostream>  
using namespace std;  
 class parent
 {  
    public:
        void parentMethod()
        {
            cout<<"I am parent class"<<endl;
        }  
 };  
class child: public parent
{
    public:
        void childMethod()
        {
            cout<<"I am child Class aquired the parent properties"<<endl;
        }
};
int main() 
{  
    child x;  
    x.parentMethod();
    x.childMethod();    
    return 0;  
}  

