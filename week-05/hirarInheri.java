class base{
    void g(){
        System.out.println("this is first class");
    }
}
class A extends base{
    void g1(){
        System.out.println("this is second class this is inherite first class");
    }
}
class C extends base{
    void g2(){
        System.out.println("this is third class it inherite second class");
    }
}
class hirarInheri{
    public static void main(String[] args) {
        C u=new C();
        A i=new A();
        u.g();
        i.g1();
        i.g1();
        u.g2();
    }
}
