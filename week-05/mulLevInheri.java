class A{
    void g(){
        System.out.println("this is first class");
    }
}
class B extends A{
    void g1(){
        System.out.println("this is second class this is inherite first class");
    }
}
class C extends B{
    void g2(){
        System.out.println("this is third class it inherite second class");
    }
}
class mulLevInheri{
    public static void main(String[] args) {
        C u=new C();
        u.g();
        u.g1();
        u.g2();
    }
}
