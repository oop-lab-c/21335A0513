#include<iostream>
using namespace std;
class demo{
public:
void hi(){
    cout<<"this is first class"<<endl;
}
};
class demo1:public demo{
public:
void hi1(){
    cout<<"this is second  class inherite class demo"<<endl;
}
};
class demo2:public demo1{
public:
void hi2(){
    cout<<"this is third class inherite class demo and demo1"<<endl;
}
};
int main(){
    demo2 d;
    d.hi();
    d.hi1();
    d.hi2();
    return 0;
}
