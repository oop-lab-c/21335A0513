import java.util.*;
interface simple {
    void man();

}

class PureAbs implements simple {
    public void man() {
        System.out.println("this is pure abstruction ");
    }

    public static void main(String[] args) {
        PureAbs u = new PureAbs();
        u.man();

    }

}
