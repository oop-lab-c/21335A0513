import java.util.*;
abstract class A{
    A(){
        System.out.println("this is a contructor:");
    }
    abstract void HI();
}
class ParAbs extends A{
    void HI() {
        System.out.println("this is partial abstruction");
    }
    public static void main(String[] args) {
        ParAbs MI = new ParAbs();
        MI.HI();
           }

}
