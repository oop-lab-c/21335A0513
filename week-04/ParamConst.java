import java.util.Scanner;

class ParamConst{
    ParamConst() 
    {
        String clgN="MVGR";
        int clgC=33;
        System.out.println("Collage DETAILS");
        System.out.println("Collage name is:"+clgN);
        System.out.println("College code is:"+clgC);
    }

    ParamConst(String name, double n) 
    {
        System.out.println("Student DETAILS:");
        System.out.println("Your name is: " + name);
        System.out.println("Your sem persentage is:"+n);
    }

    public static void main(String[] args) {
        System.out.println("Enter your name:");
        Scanner obj = new Scanner(System.in);
        String name = obj.nextLine();
        System.out.println("Enter your sem percentage:");
        double n = obj.nextDouble();
        ParamConst obj1 = new ParamConst();
        ParamConst obj2 = new ParamConst(name, n);
    }

}
